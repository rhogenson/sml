datatype D = A | B

val _ =
  case B of
    A => __builtin "exit" 0
  | B => __builtin "exit" 42
