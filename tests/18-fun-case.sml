fun fib 1 = 1
  | fib 2 = 2
  | fib n = __builtin "add" (fib (__builtin "sub" (n, 1)), fib (__builtin "sub" (n, 2)))

val _ = __builtin "exit" (__builtin "add" (fib 8, 8))
