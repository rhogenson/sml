infix 6 +
infixr 5 ::

datatype 'a list = Nil | :: of 'a * 'a list

fun x + y = __builtin "add" (x, y)

fun foldl _ acc Nil = acc
  | foldl f acc (x :: xs) = foldl f (f (x, acc)) xs

val _ = __builtin "exit" (foldl op + 0 (1 :: 2 :: 3 :: 6 :: 8 :: 10 :: 12 :: Nil))
