val _ =
  let
    val x = 42
    val y = x
  in __builtin "exit" y
  end
