val rec fib = fn n =>
  case n of
    0 => 0
  | 1 => 1
  | _ => __builtin "add" (fib (__builtin "sub" (n, 1)), fib (__builtin "sub" (n, 2)))

val _ = __builtin "exit" (__builtin "add" (fib 9, 8))
