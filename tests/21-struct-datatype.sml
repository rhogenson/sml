structure S = struct
  datatype D = D of int
end

val S.D x = S.D 42
val _ = __builtin "exit" x
