structure Compiler =
struct
  fun compile (prog : Syntax.expr) : Word8Vector.vector =
    let
      val _ = print ("ast:\n" ^ Syntax.exprToString prog ^ "\n")
      val elab = Elab.elaborate prog
      val _ = print ("lambda lang:\n" ^ Syntax.lexpToString elab ^ "\n")
      val cps =
        CPS.toCPS elab
          (fn _ => Syntax.CPrimop (Syntax.PExit, [Syntax.VInt 0], [], []))
      val _ = print ("cps1:\n" ^ Syntax.cexpToString cps ^ "\n")
      val cps' = CPS.convertClosures cps
      val _ = print ("cps:\n" ^ Syntax.cexpToString cps' ^ "\n")
      val asm = CodeGen.toASM cps'
      val _ = print ("bytecode:\n" ^ String.concatWith "\n" (map Syntax.opcodeToString asm) ^ "\n")
    in
      Linker.link asm
    end

  fun main (args : string list) : unit =
    let
      val opts = { o = ref "a.out" }
      val flags =
        [ ("o", Opts.StringOpt (fn arg => #o opts := arg)) ]
      val filename =
        case Opts.getOpt flags args of
          [arg] => arg
        | _ => raise Fail "usage: sml [-o <outfile>] <filename>"
      val ast =
        case Parser.parse filename of
          Result.Left e =>
            (print e ;
             OS.Process.exit OS.Process.failure)
        | Result.Right x => x
      val bytecode = compile ast
      val outFile = BinIO.openOut (!(#o opts))
    in
      BinIO.output (outFile, bytecode)
    end
end
