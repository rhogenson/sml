structure Result =
struct
  datatype ('a, 'b) either = Left of 'a | Right of 'b
end
