use "sort.sml";
use "result.sml";
use "buffer.sml";
use "gensym.sml";
use "map.sml";
use "syntax.sml";
use "codegen.sml";
use "cps.sml";
use "elab.sml";
use "gensym.sml";
use "linker.sml";
use "opts.sml";
use "parser.sml";
use "compiler.sml";

val _ = Compiler.main (CommandLine.arguments ())
val _ = OS.Process.exit OS.Process.success
