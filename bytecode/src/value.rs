#[derive(Debug, Clone, Copy)]
pub struct Value(pub u64);

impl Value {
    pub fn repr(self) -> u64 {
        let Value(v) = self;
        v
    }

    pub fn to_int(self) -> Option<i64> {
        if self.repr() & 1 == 0 {
            return None;
        }
        Some(self.repr() as i64 >> 1)
    }

    pub fn from_int(i: i64) -> Value {
        Value((i as u64) << 1 | 1)
    }

    pub fn to_pointer(self) -> Option<usize> {
        if self.repr() == 0 || self.repr() & 7 != 0 {
            return None;
        }
        Some(usize::try_from(self.repr() >> 3).expect("using 32 bits in 2024 LULW"))
    }

    pub fn from_pointer(p: usize) -> Value {
        Value(u64::try_from(p).expect("a usize should always fit in a u64... right?") << 3)
    }
}
