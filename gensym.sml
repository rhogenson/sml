structure Gensym =
struct
  val counter : int ref = ref 0
  fun new () : int = (counter := (!counter + 1) ; !counter)
end
