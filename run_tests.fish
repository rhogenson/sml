#!/usr/bin/env fish

set d (realpath (status dirname))

cd $d/bytecode
cargo build

cd $d

if test (count $argv) -gt 0
    set files $d/tests/$argv*
else
    set files $d/tests/*
end

for f in $files
    $d/bytecode/target/debug/bytecode (sml $d/main.sml -o /dev/stderr $f >/dev/null 2>| psub)
    set -l got $status
    if test $got -eq 42
        echo (basename -s.sml $f): PASS
    else
        echo (basename -s.sml $f): FAIL "(status $got)"
    end
end
